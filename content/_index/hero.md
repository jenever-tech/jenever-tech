+++
fragment = "hero"
#disabled = true
date = "2019-07-29"
weight = 50
background = "text_light" # can influence the text color
particles = true

title = "Jenever Tech"
subtitle = "Technical Problem Solver"

[header]
  image = "header.jpg"

[asset]
  image = "logo.png"
  width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Blog"
  url = "/blogs"
  color = "dark" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

[[buttons]]
  text = "Services"
  url = "#services"
  color = "primary"

[[buttons]]
  text = "Contact Us"
  url = "#contact"
  color = "dark"

+++
